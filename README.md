IMPORTANT:
- This is compiled with Java 1.7
- The new JUnit libraries depends on Hamcrest library from https://code.google.com/archive/p/hamcrest/downloads and get the hamcrest-all


How To Run The Application 
-Run AllTests.java as JUnit or run the ShoppingCart.java as Java application to verify the following scenarios .

	/**
	 * SCENARIO 1:
	 * 
		Items Added 			Expected Cart Total 			Expected Cart Items
		3 x Unlimited 1 GB			$94.70						3 x Unlimited 1 GB
		1 x Unlimited 5 GB										1 x Unlimited 5 GB
	 * */	 
	/**
	 * SCENARIO 2:
	 * 
		Items Added 			Expected Cart Total 			Expected Cart Items
		2 x Unlimited 1 GB			$209.4						2 x Unlimited 1 GB
		4 x Unlimited 5 GB										4 x Unlimited 5 GB
	 * */	 
	/**
	 * Scenario 3:
				Items Added 			Expected Cart Total 			Expected Cart Items
				1 x Unlimited 1 GB			$84.7						1 x Unlimited 1 GB
				2 x Unlimited 2 GB										2 x Unlimited 2 GB
																		2 x 1 Gb Data-pack
				
	 * */
	 	 	/**
	 * Scenario 4:
				Items Added 			Expected Cart Total 			Expected Cart Items
				1 x Unlimited 1 GB			$31.32						1 x Unlimited 1 GB
				1 x 1 GB Data-pack										1 x 1 Gb Data-pack
				'I<3AMAYSIM' Promo Applied														
				
	 * */