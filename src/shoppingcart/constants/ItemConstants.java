package shoppingcart.constants;


public class ItemConstants {
	
	public static final String UNLI_SMALL_CODE = "ult_small";
	public static final String UNLI_MEDIUM_CODE = "ult_medium";
	public static final String UNLI_LARGE_CODE = "ult_large";	
	public static final String DATAPACK_1GB_CODE = "1gb";	
	
	public static final double UNLI_SMALL_PRICE = 24.9;
	public static final double UNLI_MEDIUM_PRICE = 29.9;
	public static final double UNLI_LARGE_PRICE = 44.9;
	public static final double DATAPACK_1GB_PRICE= 9.9;
	
	//3for2: For every 3unlimited 1Gb, pay for the price of 2
	public static final int BUNDLE_SIZE = 3;
	public static final int PAID_SIZE = 2;
	
	//Bulk discount: If more than three Unlimited 5Gb sim,the price will drop to $39.90 each for the first month
	public static final int MIN_BULK_DISCOUNT = 3;
	public static final double BULK_NEWPRICE_EACH = 39.9;

	//promo code 'I<3AMAYSIM' will apply a 10% discount across the board.
	public static final String PROMO_AMAYSIM = "I<3AMAYSIM";
	public static final double PROMO_AMAYSIM_DISCOUNT = 10;

}
