package shoppingcart.test;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import shoppingcart.constants.ItemConstants;
import shoppingcart.core.Item;
import shoppingcart.core.ItemBean;
import shoppingcart.core.ShoppingCart;
import shoppingcart.pricingrules.PricingRules;

public class ShoppingCartTest {
	
	private static ShoppingCart cart;
	private static PricingRules pricingRules;
	private static Item lineItem1;
	double expectedTotalPrice = ItemConstants.UNLI_SMALL_PRICE;
	
    @BeforeClass
    public static void setUp() {
    	pricingRules = new PricingRules();
        cart = new ShoppingCart(pricingRules);          
        lineItem1 = new ItemBean(ItemConstants.UNLI_SMALL_CODE, "Unlimited 1 GB", ItemConstants.UNLI_SMALL_PRICE, 1);        
    	cart.add(lineItem1);    	 
    	cart.computeTotalPrice();
    }
        
    @Test
    public void testAddItem(){
    	assertTrue(cart.hasItem(lineItem1));
    }
    
    @Test
    public void testComputeTotal(){
    	assertEquals(expectedTotalPrice,cart.getTotalPrice(), 1.0);
    }

}
