package shoppingcart.test;

import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

import shoppingcart.constants.ItemConstants;
import shoppingcart.core.Item;
import shoppingcart.core.ItemBean;
import shoppingcart.core.ShoppingCart;
import shoppingcart.pricingrules.PricingRules;


public class Scenario3Test {
	
	private static ShoppingCart cart;
	private static PricingRules pricingRules;
	private static Item lineItem1, lineItem2;
	private double expectedCartTotal = 84.7;
	private int expectedCartItems = 3;

	/**
	 * Scenario 3:
				Items Added 			Expected Cart Total 			Expected Cart Items
				1 x Unlimited 1 GB			$84.7						1 x Unlimited 1 GB
				2 x Unlimited 2 GB										2 x Unlimited 2 GB
																		2 x 1 Gb Data-pack
				
	 * */
	
    @BeforeClass
    public static void setUp() {
    	pricingRules = new PricingRules();
        cart = new ShoppingCart(pricingRules);          
        lineItem1 = new ItemBean(ItemConstants.UNLI_SMALL_CODE, "Unlimited 1 GB", ItemConstants.UNLI_SMALL_PRICE, 1);
        lineItem2 = new ItemBean(ItemConstants.UNLI_MEDIUM_CODE, "Unlimited 2 GB", ItemConstants.UNLI_MEDIUM_PRICE, 2);
    	cart.add(lineItem1);    	
    	cart.add(lineItem2); 
    	cart.computeTotalPrice();
    }
    
    @Test
    public void testCartTotal(){    	    	    	   	
    	assertEquals(expectedCartTotal,cart.getTotalPrice(), 1.0);
    }
    
    @Test
    public void testCartNumberItems(){    	  	
    	assertEquals(expectedCartItems, cart.getListItems().size());
    }    
    
    @Test
    public void testCartItems(){
    	 assertEquals("1    X    Unlimited 1 GB \n" +
					  "2    X    1Gb Data Pack \n" +
					  "2    X    Unlimited 2 GB \n" +
					  "-------------------------------------\n" +
					  "TOTAL PRICE:         $84.70",
    			 cart.printListItems());
    }    
}

