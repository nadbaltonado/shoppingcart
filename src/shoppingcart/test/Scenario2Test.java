package shoppingcart.test;

import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

import shoppingcart.constants.ItemConstants;
import shoppingcart.core.Item;
import shoppingcart.core.ItemBean;
import shoppingcart.core.ShoppingCart;
import shoppingcart.pricingrules.PricingRules;


public class Scenario2Test {
	
	private static ShoppingCart cart;
	private static PricingRules pricingRules;
	private static Item lineItem1, lineItem2;
	private double expectedCartTotal = 209.4;
	private int expectedCartItems = 2;

	/**
	 * SCENARIO 2:
	 * 
		Items Added 			Expected Cart Total 			Expected Cart Items
		2 x Unlimited 1 GB			$209.4						2 x Unlimited 1 GB
		4 x Unlimited 5 GB										4 x Unlimited 5 GB
	 * */
	
    @BeforeClass
    public static void setUp() {
    	pricingRules = new PricingRules();
        cart = new ShoppingCart(pricingRules);          
        lineItem1 = new ItemBean(ItemConstants.UNLI_SMALL_CODE, "Unlimited 1 GB", ItemConstants.UNLI_SMALL_PRICE, 2);
        lineItem2 = new ItemBean(ItemConstants.UNLI_LARGE_CODE, "Unlimited 5 GB", ItemConstants.UNLI_LARGE_PRICE, 4);
    	cart.add(lineItem1);    	
    	cart.add(lineItem2); 
    	cart.computeTotalPrice();
    }
    
    @Test
    public void testCartTotal(){    	    	    	   	
    	assertEquals(expectedCartTotal,cart.getTotalPrice(), 1.0);
    }
    
    @Test
    public void testCartNumberItems(){
    	assertEquals(expectedCartItems, cart.getListItems().size());
    }    
    
    @Test
    public void testCartItems(){
    	 assertEquals("2    X    Unlimited 1 GB \n" +
					  "4    X    Unlimited 5 GB \n" +
					  "-------------------------------------\n" +
					  "TOTAL PRICE:         $209.40",
    			 cart.printListItems());
    }    
}
