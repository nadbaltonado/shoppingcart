package shoppingcart.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite.SuiteClasses;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@SuiteClasses( { Scenario1Test.class,
				 Scenario2Test.class,
				 Scenario3Test.class,
				 Scenario4Test.class}
			 )
public class AllTests {

}
