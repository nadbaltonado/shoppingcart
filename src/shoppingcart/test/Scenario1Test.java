package shoppingcart.test;

import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

import shoppingcart.constants.ItemConstants;
import shoppingcart.core.Item;
import shoppingcart.core.ItemBean;
import shoppingcart.core.ShoppingCart;
import shoppingcart.pricingrules.PricingRules;


public class Scenario1Test {
	
	private static ShoppingCart cart;
	private static PricingRules pricingRules;
	private static Item lineItem1, lineItem2;
	private double expectedCartTotal = 94.7;
	private int expectedCartItems = 2;

	/**
	 * SCENARIO 1:
	 * 
		Items Added 			Expected Cart Total 			Expected Cart Items
		3 x Unlimited 1 GB			$94.70						3 x Unlimited 1 GB
		1 x Unlimited 5 GB										1 x Unlimited 5 GB
	 * */
	
    @BeforeClass
    public static void setUp() {
    	pricingRules = new PricingRules();
        cart = new ShoppingCart(pricingRules);          
        lineItem1 = new ItemBean(ItemConstants.UNLI_SMALL_CODE, "Unlimited 1 GB", ItemConstants.UNLI_SMALL_PRICE, 3);
        lineItem2 = new ItemBean(ItemConstants.UNLI_LARGE_CODE, "Unlimited 5 GB", ItemConstants.UNLI_LARGE_PRICE, 1);
    	cart.add(lineItem1);    	
    	cart.add(lineItem2); 
    	cart.computeTotalPrice();
    }
    
    @Test
    public void testCartTotal(){    	    	    	   	
    	assertEquals(expectedCartTotal,cart.getTotalPrice(), 1.0);
    }
    
    @Test
    public void testCartNumberItems(){
    	assertEquals(expectedCartItems, cart.getListItems().size(), 1);
    }    
    
    @Test
    public void testCartItems(){
    	 assertEquals("3    X    Unlimited 1 GB \n" +
					  "1    X    Unlimited 5 GB \n" +
					  "-------------------------------------\n" +
					  "TOTAL PRICE:         $94.70",
    			 cart.printListItems());
    }    
}
