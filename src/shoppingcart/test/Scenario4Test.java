package shoppingcart.test;

import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;

import shoppingcart.constants.ItemConstants;
import shoppingcart.core.Item;
import shoppingcart.core.ItemBean;
import shoppingcart.core.ShoppingCart;
import shoppingcart.pricingrules.PricingRules;


public class Scenario4Test {
	
	private static ShoppingCart cart;
	private static PricingRules pricingRules;
	private static Item lineItem1, lineItem2;
	private double expectedCartTotal = 31.32;
	private int expectedCartItems = 2;
	
	private  String template = "1    X    Unlimited 1 GB    %s \n" +
				  "1    X    1 GB Data-pack    %s \n" +
				  "I<3AMAYSIM Promo Applied \n" +
				  "-------------------------------------\n" +
				  "TOTAL PRICE:          %s";

	/**
	 * Scenario 4:
				Items Added 			Expected Cart Total 			Expected Cart Items
				1 x Unlimited 1 GB			$31.32						1 x Unlimited 1 GB
				1 x 1 GB Data-pack										1 x 1 Gb Data-pack
				'I<3AMAYSIM' Promo Applied														
				
	 * */
	
    @BeforeClass
    public static void setUp() {
    	pricingRules = new PricingRules();
        cart = new ShoppingCart(pricingRules);          
        lineItem1 = new ItemBean(ItemConstants.UNLI_SMALL_CODE, "Unlimited 1 GB", ItemConstants.UNLI_SMALL_PRICE, 1);
        lineItem2 = new ItemBean(ItemConstants.DATAPACK_1GB_CODE, "1 GB Data-pack", ItemConstants.DATAPACK_1GB_PRICE, 1);
        cart.setCartPromoCode(ItemConstants.PROMO_AMAYSIM);
    	cart.add(lineItem1);    	
    	cart.add(lineItem2); 
    	cart.computeTotalPrice();
    }
    
    @Test
    public void testCartTotal(){    	    	    	   	
    	assertEquals(expectedCartTotal,cart.getTotalPrice(), 1.0);
    }
    
    @Test
    public void testCartNumberItems(){
    	assertEquals(expectedCartItems, cart.getListItems().size());
    }    
    
    @Test
    public void testCartItems(){
    	 assertEquals("1    X    Unlimited 1 GB \n" +
					  "1    X    1 GB Data-pack \n" +
					  "I<3AMAYSIM Promo Applied \n" +
					  "-------------------------------------\n" +
					  "TOTAL PRICE:         $31.32",
    			 cart.printListItems());
    }
    
    //@Test
    public void testCartItems_withTemplate(){
    	String expected = String.format(template, "24.9", "9.9", "31.32");
    	 assertEquals(expected, cart.printListItems());
    }  
}

