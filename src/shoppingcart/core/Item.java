package shoppingcart.core;

public interface Item {
	
	public double getPriceByQuantity();
	public int getQuantity();
	public double getUnitPrice();
	public String getProductName();
	public String getProductCode();
	public void setQuantity(int i);
}
