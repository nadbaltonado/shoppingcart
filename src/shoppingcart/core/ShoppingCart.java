package shoppingcart.core;

import java.util.ArrayList;

import shoppingcart.constants.ItemConstants;
import shoppingcart.pricingrules.PricingRules;


public class ShoppingCart {
	
	public ArrayList<Item> listItems;
	PricingRules pricingRules;
	double totalPrice;
	String promoCode;
	
	public ShoppingCart(PricingRules pricingRules){
		listItems = new ArrayList<Item>();
		this.pricingRules = pricingRules;
	}
	
	public void add(Item item){		
		item = pricingRules.applyValidPromo(item, this);
		if (listItems.contains(item)){
			item.setQuantity(item.getQuantity()+1);
		}else{
			listItems.add(item);
		}		
	}
	
	public void removeItem(Item item){
		listItems.remove(item);
	}
	
	/*
	//This method will have the promo apply to this item only
	public void add(Item item, String promoCode){
		this.promoCode = promoCode;
		add(item);
	}
	*/
	
	public double computeTotalPrice(){		
		for(Item item: listItems){						
			totalPrice+= item.getPriceByQuantity();
		}
		//apply promo code for the total cart
		if (promoCode != null && promoCode.equals(ItemConstants.PROMO_AMAYSIM)){
			totalPrice = totalPrice - (totalPrice * (ItemConstants.PROMO_AMAYSIM_DISCOUNT/100));
		}		
		return totalPrice;
	}
	
	public double getTotalPrice(){
		return totalPrice;
	}
	
	public void setCartPromoCode(String promoCode){
		this.promoCode = promoCode;
	}
	
	public ArrayList<Item> getListItems(){			
		return this.listItems;
	}
	
	public boolean hasItem(Item itemToCheck){
		for(Item item: listItems){						
			return (item.getProductCode().equals(itemToCheck.getProductCode()));
		}		
		return false;
	}
	
	public String printListItems(){
		String space="    ";
		String nextLine=" \n";
		StringBuilder sb = new StringBuilder();
		for(Item item: listItems){			
			sb.append(item.getQuantity()).append(space).append("X").append(space).append(item.getProductName());	
			sb.append(nextLine);
		}
		if (promoCode != null && !promoCode.equals("")){
			sb.append(promoCode+" Promo Applied").append(nextLine);
		}
		sb.append("-------------------------------------\n");
		sb.append("TOTAL PRICE:         $"+String.format("%.2f", getTotalPrice()));
		System.out.println(sb.toString());
		return sb.toString();
		
	}



	/**For quick testing only
	 * */
	public static void main(String[] args){
			
		PricingRules pricingRules = new PricingRules();		
		
		/**
		 * Scenario 1:
					Items Added 			Expected Cart Total 			Expected Cart Items
					3 x Unlimited 1 GB			$94.70						3 x Unlimited 1 GB
					1 x Unlimited 5 GB										1 x Unlimited 5 GB
		 * */	
		ShoppingCart cart1 = new ShoppingCart(pricingRules);
        Item lineItem1 = new ItemBean(ItemConstants.UNLI_SMALL_CODE, "Unlimited 1 GB", ItemConstants.UNLI_SMALL_PRICE, 3);
        Item lineItem2 = new ItemBean(ItemConstants.UNLI_LARGE_CODE, "Unlimited 5 GB", ItemConstants.UNLI_LARGE_PRICE, 1);		
        cart1.add(lineItem1);
        cart1.add(lineItem2);    	      		
        cart1.computeTotalPrice();
        System.out.println("\n SCENARIO 1:");
        cart1.printListItems();
       
		
		/**
		 * Scenario 2:
					Items Added 			Expected Cart Total 			Expected Cart Items
					2 x Unlimited 1 GB			$209.4						2 x Unlimited 1 GB
					4 x Unlimited 5 GB										4 x Unlimited 5 GB
		 * */	
		ShoppingCart cart2 = new ShoppingCart(pricingRules);
        Item lineItem3 = new ItemBean(ItemConstants.UNLI_SMALL_CODE, "Unlimited 1 GB", ItemConstants.UNLI_SMALL_PRICE, 2);
        Item lineItem4 = new ItemBean(ItemConstants.UNLI_LARGE_CODE, "Unlimited 5 GB", ItemConstants.UNLI_LARGE_PRICE, 4);		
        cart2.add(lineItem3);
        cart2.add(lineItem4);    	      		
        cart2.computeTotalPrice();
        System.out.println("\n SCENARIO 2:");
        cart2.printListItems();
        
     
		/**
		 * Scenario 3:
					Items Added 			Expected Cart Total 			Expected Cart Items
					1 x Unlimited 1 GB			$84.7						1 x Unlimited 1 GB
					2 x Unlimited 2 GB										2 x Unlimited 2 GB
																			2 x 1 Gb Data-pack
					
		 * */	
		ShoppingCart cart3 = new ShoppingCart(pricingRules);
        Item lineItem5 = new ItemBean(ItemConstants.UNLI_SMALL_CODE, "Unlimited 1 GB", ItemConstants.UNLI_SMALL_PRICE, 1);
        Item lineItem6 = new ItemBean(ItemConstants.UNLI_MEDIUM_CODE, "Unlimited 2 GB", ItemConstants.UNLI_MEDIUM_PRICE, 2);		
        cart3.add(lineItem5);
        cart3.add(lineItem6);    	      		
        cart3.computeTotalPrice();
        System.out.println("\n SCENARIO 3:");
        cart3.printListItems();    
        
		/**
		 * Scenario 4:
					Items Added 			Expected Cart Total 			Expected Cart Items
					1 x Unlimited 1 GB			$31.32						1 x Unlimited 1 GB
					1 x 1 GB Data-pack										1 x 1 GB Data-pack
					'I<3AMAYSIM' Promo Applied														
					
		 * */	
		ShoppingCart cart4 = new ShoppingCart(pricingRules);
        Item lineItem7 = new ItemBean(ItemConstants.UNLI_SMALL_CODE, "Unlimited 1 GB", ItemConstants.UNLI_SMALL_PRICE, 1);
        Item lineItem8 = new ItemBean(ItemConstants.DATAPACK_1GB_CODE, "1 GB Data-pack", ItemConstants.DATAPACK_1GB_PRICE, 1);		
        cart4.add(lineItem7);
        cart4.add(lineItem8);
        cart4.setCartPromoCode(ItemConstants.PROMO_AMAYSIM);
        cart4.computeTotalPrice();
        System.out.println("\n SCENARIO 4:");
        cart4.printListItems();         
        
	}
	
}
