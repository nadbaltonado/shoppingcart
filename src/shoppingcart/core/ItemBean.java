package shoppingcart.core;

public class ItemBean implements Item{
	
	private String productCode;
	private String productName;
	private double unitPrice;
	private int quantity;
	
	public ItemBean(String productCode, String productName, double unitPrice, int quantity){
		this.productCode = productCode;
		this.productName = productName;
		this.unitPrice = unitPrice;
		this.quantity = quantity;
	}
	

	public String getProductCode() {
		return productCode;
	}


	public String getProductName() {
		return productName;
	}

	public double getUnitPrice() {
		return unitPrice;
	}

	public int getQuantity() {
		return quantity;
	}

	public double getPriceByQuantity() {
		return unitPrice * quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	@Override
	public String toString(){
		String space="      ";
		StringBuilder sb = new StringBuilder();
		sb.append(quantity).append(space).append(productName).append(space).append(unitPrice);
		return sb.toString();
	}

	@Override public boolean equals(Object obj) { 
		if (obj == this) { return true; } 
		if (obj == null || obj.getClass() != this.getClass()) { return false; } 
		ItemBean item = (ItemBean) obj; 
		return productCode.equals(item.getProductCode()); 
	} 

	@Override 
	public int hashCode() { 
		final int prime = 62; 
		int result = 1; 
		result = prime * result + ((productCode == null) ? 0 : productCode.hashCode()); 
		return result; 
	} 

	

}
