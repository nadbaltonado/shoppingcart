package shoppingcart.pricingrules;

import shoppingcart.core.Item;
import shoppingcart.core.ShoppingCart;

public class FreeItemPricing extends Pricing {
	Item freeItem;
	ShoppingCart cart;
	
	public FreeItemPricing(Item item, int forEveryNumItem, Item freeItem, ShoppingCart cart){
		super(item);
		this.freeItem = freeItem;
		this.cart= cart;
		int numFreeItem = item.getQuantity()/forEveryNumItem;
		freeItem.setQuantity(numFreeItem);
		cart.add(freeItem);
	}
	
	@Override
	public double getPriceByQuantity(){
		return baseItem.getPriceByQuantity();
	}	

}
