package shoppingcart.pricingrules;

import shoppingcart.core.Item;

public class BundlePricing extends Pricing {
	
	int bundleSize;
	double paidSize;
	
	public BundlePricing(Item item, int bundleSize, int paidSize){
		super(item);
		this.bundleSize = bundleSize;
		this.paidSize = paidSize;
	}
	
	/** numBundles will be computed based on (paidSize * unitPrice)
	 * The remaining will have the regular price for each
	 * **/
	@Override
	public double getPriceByQuantity(){
		int numBundles = super.getQuantity()/bundleSize;
		int remainingItems = super.getQuantity() % bundleSize;
		return numBundles*(paidSize * super.getUnitPrice()) + (remainingItems * super.getUnitPrice());
	}	

}
