package shoppingcart.pricingrules;

import shoppingcart.constants.ItemConstants;
import shoppingcart.core.Item;
import shoppingcart.core.ItemBean;
import shoppingcart.core.ShoppingCart;
import shoppingcart.pricingrules.BulkDiscountPricing;
import shoppingcart.pricingrules.BundlePricing;
import shoppingcart.pricingrules.FreeItemPricing;
import shoppingcart.pricingrules.PricingRules;


public class PricingRules {
				
	//FreeItem: free 1 GB Data-pack free-of-charge with every Unlimited 2GB sold.Price is 0 since it's free
	Item freeItem = new ItemBean("1Gb", "1Gb Data Pack", 0, 1);
	int forEveryNumItem=1;
	
	
	public Item applyValidPromo(Item item, ShoppingCart cart){		
		String productCode = item.getProductCode();
		
		if (productCode.equals(ItemConstants.UNLI_SMALL_CODE)){
			item = new BundlePricing(item, ItemConstants.BUNDLE_SIZE, ItemConstants.PAID_SIZE);
			
		}else if (productCode.equals(ItemConstants.UNLI_MEDIUM_CODE)){
			item = new FreeItemPricing(item, forEveryNumItem, freeItem, cart);
			
		}else if (productCode.equals(ItemConstants.UNLI_LARGE_CODE)){
			item = new BulkDiscountPricing(item, ItemConstants.MIN_BULK_DISCOUNT, ItemConstants.BULK_NEWPRICE_EACH);
		}
				
		return item;
	}

	

}
