package shoppingcart.pricingrules;

import shoppingcart.core.Item;

public class Pricing implements Item {
	
	Item baseItem;
	
	public Pricing(Item item){
		baseItem = item;
	}
	
	@Override
	public double getPriceByQuantity(){
		return baseItem.getPriceByQuantity();
	}
	
	@Override
	public int getQuantity(){
		return baseItem.getQuantity();
	}	
	
	@Override
	public double getUnitPrice(){
		return baseItem.getUnitPrice();
	}
	
	@Override
	public String getProductName(){
		return baseItem.getProductName();
	}

	@Override
	public String getProductCode(){
		return baseItem.getProductCode();
	}
	
	public void setQuantity(int quantity) {
		baseItem.setQuantity(quantity);
	}	
}
