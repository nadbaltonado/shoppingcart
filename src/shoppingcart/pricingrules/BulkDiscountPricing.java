package shoppingcart.pricingrules;

import shoppingcart.core.Item;

public class BulkDiscountPricing extends Pricing {
	
	int minSizeForDiscount;
	double newPriceForEach;
	
	public BulkDiscountPricing(Item item, int minimumSizeForDiscount, double newPriceForEach){
		super(item);		
		this.minSizeForDiscount = minimumSizeForDiscount;
		this.newPriceForEach = newPriceForEach;
	}
	
	@Override
	public double getPriceByQuantity(){
		double price=super.getPriceByQuantity();
		if (super.getQuantity()>minSizeForDiscount){
			//price = (super.getUnitPrice()-(super.getUnitPrice()*discountRate))* super.getQuantity();
			price = super.getQuantity() * newPriceForEach;
		}
		return price;
	}	

}
