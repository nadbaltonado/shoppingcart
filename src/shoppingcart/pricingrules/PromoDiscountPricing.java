package shoppingcart.pricingrules;

import shoppingcart.core.Item;

public class PromoDiscountPricing extends Pricing  {

	double discountRate;
	
	public PromoDiscountPricing(Item item, double discountRate){
		super(item);
		
        if (discountRate < 0 || discountRate > 100 ) {
            throw new IllegalArgumentException("Discount Rate must be in [0,100]");
        }
		this.discountRate=discountRate/100;
	}
	
	@Override
	public double getPriceByQuantity(){
		return baseItem.getPriceByQuantity()- (baseItem.getPriceByQuantity() * discountRate);
	}
}
